FROM debian:buster as helm-builder

ARG BASE_VERSION

ENV HELM_VERSION=v$BASE_VERSION

WORKDIR /root/

RUN apt-get update \
 && apt-get install -y curl \
 && curl -Ls https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz 

FROM mdeterman/k8-kubectl

RUN apt-get update \
 && apt-get install -y git curl gsutil \
 && rm -rf /var/lib/apt/lists/*

COPY --from=helm-builder /root/linux-amd64/helm /usr/local/bin
RUN helm version --client


RUN mkdir -p /etc/deploy && \
    helm repo add stable https://charts.helm.sh/stable && \
    helm repo add io-determan https://io-determan-charts.storage.googleapis.com

CMD ["helm"]
